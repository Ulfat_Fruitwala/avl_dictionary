# AVL Dictionary
----------------
AVL Dictionary is a fast data structure for inserting words and/or retrieving number of words lexographically between two strings. It is capable of performing 4 million queries in under 15 seconds.

## Files
--------
These are the files associated with the program

- Code files: Makefile, main.cpp, avl.cpp, avl.h
- Extra files: README

## Usage
--------
- Run "make", to get executable "wordrange"
- Run "./wordrange <INPUT FILE> <OUTPUT FILE>"

Input files should have two operations, i and r. Use "i <string>" to insert a string into the data structure. Use r <string1> <string2> to get number of values currently in the data structure that are lexographically between the two string values in the input file. Note that each operation must be on a newline and string1 must be less than string2.

## Built Using
-------------
- C++

## Reference
------------
Code for insert, balance, rotate_right, and rotate_left is recreated from this [link](https://www.geeksforgeeks.org/avl-tree-set-1-insertion/). Additional code in the functions was written to reconnect child and parent pointer since the reference didn't implement a parent pointer of each node.

## Author
---------
- Ulfat Fruitwala